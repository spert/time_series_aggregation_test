﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using TimeSeriesTest;
using BenchmarkDotNet.Running;


namespace BenchmarkingLib
{
    public class Program
    {
        static List<TseriesDict> dailyDictSeries = new List<TseriesDict>();
        static List<TseriesOb> dailyObjectSeries = new List<TseriesOb>();

        static Program()
        {
            for (int i = 0; i < 61; i++)
            {
                DateTime firstPeriod = new DateTime(1970, 1, 5);
                DateTime lastPeriod = new DateTime(2050, 12, 25);
                Freq f = Freq.Day;
                Transf t = Transf.Average;

                TseriesDict serDict = HelperMethods.GenerateDictSeries(firstPeriod, lastPeriod, f, t);
                TseriesOb serObject = HelperMethods.ConvertDictSeriesToObjectSeries(serDict);

                dailyDictSeries.Add(serDict);
                dailyObjectSeries.Add(serObject);
            }

        }

        [Benchmark]
        public void DictSpeedTest()
        {
            List<TseriesDict> resDict = new List<TseriesDict>();

            foreach (var ser in dailyDictSeries)
            {
                var monthlyDictSeries = TSeriesConverter.AggregateByDictionary(ser, Freq.Month);
                resDict.Add(monthlyDictSeries);
            }

        }

        [Benchmark]
        public void HeapSpeedTest()
        {
            List<TseriesOb> resOb = new List<TseriesOb>();

            foreach (var ser in dailyObjectSeries)
            {
                var monthlyObjectSeries = TSeriesConverter.AggregateInHeap(ser, Freq.Month);
                resOb.Add(monthlyObjectSeries);
            }
        }

        [Benchmark]
        public void StackSpeedTest()
        {
            List<TseriesOb> resOb = new List<TseriesOb>();

            foreach (var ser in dailyObjectSeries)
            {
                var monthlyObjectSeries = TSeriesConverter.AggregateInStack(ser, Freq.Month);
                resOb.Add(monthlyObjectSeries);
            }
        }
        

        static void Main(string[] args)
        {

            var summary = BenchmarkRunner.Run<Program>();

            Console.ReadLine();
            
        }
    }
}
