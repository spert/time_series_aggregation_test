﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using TimeSeriesTest;

namespace TimeSeriesTest
{
    public static class HelperMethods
    {
        public static TseriesDict GenerateDictSeries(DateTime firstPeriod, DateTime lastPeriod, Freq Frequency, Transf trans)
        {
            Dictionary<DateTime, double> dt = new Dictionary<DateTime, double>();
            int start = 0;

            Random r = new Random();

            if (Frequency == Freq.Day)
            {
                start = (firstPeriod - new DateTime(1970, 1, 1)).Days + 1;
                int daysToEnd = (lastPeriod - firstPeriod).Days;

                for (int i = 0; i <= daysToEnd; i++)
                {
                    if (firstPeriod.AddDays(i).DayOfWeek == DayOfWeek.Saturday || firstPeriod.AddDays(i).DayOfWeek == DayOfWeek.Sunday)
                        dt.Add(firstPeriod.AddDays(i), double.NaN);
                    else
                        dt.Add(firstPeriod.AddDays(i), Math.Round((r.NextDouble() * 100000)) / 100);
                }
            }

            if (Frequency == Freq.Month)
            {
                start = firstPeriod.MonthlyDifference(new DateTime(1970, 1, 1)) + 1;
                int monthsToEnd = lastPeriod.MonthlyDifference(firstPeriod);

                for (int i = 0; i <= monthsToEnd; i++)
                {
                    dt.Add(firstPeriod.AddMonths(i).GetLastDayOfMonth(0), (r.NextDouble() * 100000) / 100);
                }
            }

            if (Frequency == Freq.Quarter)
            {
                start = firstPeriod.QuarterlyDifference(new DateTime(1970, 1, 1)) + 1;
                int quarters = lastPeriod.QuarterlyDifference(firstPeriod);

                for (int i = 0; i <= quarters; i++)
                {
                    dt.Add(firstPeriod.AddMonths(i * 3).GetLastDayOfQuarter(0), (r.NextDouble() * 100000) / 100);
                }
            }

            if (Frequency == Freq.Year)
            {
                start = firstPeriod.YearlyDifference(new DateTime(1970, 1, 1)) + 1;
                var years = lastPeriod.YearlyDifference(firstPeriod);

                for (int i = 0; i <= years; i++)
                {
                    dt.Add(firstPeriod.AddMonths(i * 12).GetLastDayOfYear(0), (r.NextDouble() * 100000) / 100);
                }
            }

            TseriesDict ts = new TseriesDict();
            ts.Freq = Frequency;
            ts.ShortName = "abc";
            ts.Transf = trans;
            ts.LastUpdated = new DateTime(1970, 1, 1);
            ts.Obs = dt;

            return ts;
        }

        public static void ExportSeriesToCsvTable(List<TseriesDict> dailyDictSeries, string filePath)
        {
            Dictionary<DateTime, string> result = new Dictionary<DateTime, string>();

            var dates = dailyDictSeries.SelectMany(x => x.Obs.Keys).Distinct().OrderBy(k => k).ToList();

            foreach (var d in dates)
                result.Add(d, "");

            foreach (var d in dates)
            {
                for (int i = 0; i < dailyDictSeries.Count; i++)
                {
                    if (dailyDictSeries[i].Obs.ContainsKey(d))
                        result[d] = result[d] + ";" + dailyDictSeries[i].Obs[d].ToString();
                    else
                        result[d] = result[d] + ";NaN";
                }
            }

            string header = "dates";
            for (int i = 0; i < dailyDictSeries.Count; i++)
            {
                header = header + ";s" + i.ToString();
            }

            String csv = String.Join(Environment.NewLine, result.Select(d => d.Key.ToShortDateString() + d.Value));
            System.IO.File.WriteAllText(filePath, string.Concat(header, Environment.NewLine, csv));
                                 
        }


        public static TseriesOb ConvertDictSeriesToObjectSeries(TseriesDict t)
        {
            TseriesOb ts = new TseriesOb();
            ts.Freq = t.Freq;
            ts.Transf = t.Transf;
            ts.FirstPeriod = t.FirstPeriod;
            ts.Obs = (new List<double>(t.Obs.Values)).ToArray();

            return ts;
        }

 
        public static bool TimeSeriesAreEqual(TseriesOb t1, TseriesOb t2)
        {

            if (t1.FirstPeriod != t2.FirstPeriod)
                throw new Exception("First period is difefrent: " + t1.FirstPeriod + " / " + t2.FirstPeriod);

            if (t1.Obs.Length != t2.Obs.Length)
                throw new Exception("Number of observations is diffrent: " + t1.Obs.GetUpperBound(0) + " / " + t2.Obs.GetUpperBound(0));

            for (int i = 0; i < t1.Obs.Length; i++)
            {
                if (Math.Round(t1.Obs[i],4) != Math.Round(t2.Obs[i],4))
                    throw new Exception("Observation number " + i + "  is diffrent");
            }

            if (t1.Freq != t2.Freq)
                throw new Exception("Frequency is diffrent" + t1.Freq + " / " + t2.Freq);

            if (t1.Transf != t2.Transf)
                throw new Exception("Tranformations is diffrent" + t1.Transf + " / " + t2.Transf);

            if (t1.FirstPeriod != t2.FirstPeriod)
                throw new Exception("Firstperiod is diffrent" + t1.Transf + " / " + t2.Transf);

            if (t1.LastPeriod != t2.LastPeriod)
                throw new Exception("Lastperiod is diffrent" + t1.Transf + " / " + t2.Transf);

            if (t1.FirstPeriodDate != t2.FirstPeriodDate)
                throw new Exception("FirstPeriodDate is diffrent" + t1.Transf + " / " + t2.Transf);

            if (t1.LastPeriodDate != t2.LastPeriodDate)
                throw new Exception("LastPeriodDate is diffrent" + t1.Transf + " / " + t2.Transf);

            return true;

        }

        public static bool TimeSeriesAreEqual(TseriesDict t1, TseriesOb t2)
        {
            double[] dict = (new List<double>(t1.Obs.Values)).ToArray();

            if (dict.Length != t2.Obs.Length)
                throw new Exception("Number of observations is diffrent: " + dict.GetUpperBound(0) + " / " + t2.Obs.GetUpperBound(0));

            for (int i = 0; i < dict.Length; i++)
            {
                if (!double.IsNaN(dict[i]) && !double.IsNaN(t2.Obs[i]))
                {
                    if (Math.Round(dict[i],4) != Math.Round(t2.Obs[i],4))
                        throw new Exception("Observation number " + i + "  is diffrent");
                } 
            }

            if (t1.Freq != t2.Freq)
                throw new Exception("Frequency is diffrent" + t1.Freq + " / " + t2.Freq);

            if (t1.Transf != t2.Transf)
                throw new Exception("Tranformations is diffrent" + t1.Transf + " / " + t2.Transf);

            if (t1.FirstPeriod != t2.FirstPeriod)
                throw new Exception("Firstperiod is diffrent" + t1.Transf + " / " + t2.Transf);

            if (t1.LastPeriod != t2.LastPeriod)
                throw new Exception("Lastperiod is diffrent" + t1.Transf + " / " + t2.Transf);

            if (t1.FirstPeriodDate != t2.FirstPeriodDate)
                throw new Exception("FirstPeriodDate is diffrent" + t1.Transf + " / " + t2.Transf);

            if (t1.LastPeriodDate != t2.LastPeriodDate)
                throw new Exception("LastPeriodDate is diffrent" + t1.Transf + " / " + t2.Transf);

            return true;

        }

        public static bool TimeSeriesAreEqual(TseriesOb t2, TseriesDict t1)
        {
            return TimeSeriesAreEqual(t2, t1);
        }
       
    }
}
