﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeSeriesTest
{
    public static class TSeriesExtensions
    {
        public static DateTime GetLastDayOfMonth(this DateTime dateTime, int months)
        {
            return ((new DateTime(dateTime.Year, dateTime.Month, 1)).AddMonths(1 + months).AddDays(-1));
        }
        
        public static DateTime GetFirstDayOfMonth(this DateTime dateTime, int months)
        {
            return (new DateTime(dateTime.Year, dateTime.Month, 1)).AddMonths(months);
        }

        public static DateTime GetLastDayOfQuarter(this DateTime dateTime, int quarters)
        {
            DateTime _dateTime = dateTime.GetLastDayOfMonth(3 * quarters);
            int m = 0;

            if (_dateTime.Month >= 1 && _dateTime.Month <= 3)
                m = 3;
            else if (_dateTime.Month >= 4 && _dateTime.Month <= 6)
                m = 6;
            else if (_dateTime.Month >= 7 && _dateTime.Month <= 9)
                m = 9;
            else if (_dateTime.Month >= 10 && _dateTime.Month <= 12)
                m = 12;

            return new DateTime(_dateTime.Year, m, DateTime.DaysInMonth(_dateTime.Year, m));
        }

        public static DateTime GetFirstDayOfQuarter(this DateTime dateTime, int quarters)
        {
            DateTime _dateTime = dateTime.GetFirstDayOfMonth(3 * quarters);
            int m = 0;

            if (_dateTime.Month >= 1 && _dateTime.Month <= 3)
                m = 1;
            else if (_dateTime.Month >= 4 && _dateTime.Month <= 6)
                m = 4;
            else if (_dateTime.Month >= 7 && _dateTime.Month <= 9)
                m = 7;
            else if (_dateTime.Month >= 10 && _dateTime.Month <= 12)
                m = 10;

            return new DateTime(_dateTime.Year, m, 1);
        }
        
        public static DateTime GetLastDayOfYear(this DateTime dateTime, int years)
        {
            return new DateTime(dateTime.Year + years, 12, 31);
        }

        public static DateTime GetFirstDayOfYear(this DateTime dateTime, int years)
        {
            return new DateTime(dateTime.Year + years, 1, 1);
        }

        public static int DailyDifference(this DateTime lValue, DateTime rValue)
        {
            return (lValue - rValue).Days;
        }

        public static int DailyDifferenceFull(this DateTime lValue, DateTime rValue)
        {
            return DailyDifference(lValue, rValue);
        }

        public static int MonthlyDifference(this DateTime lValue, DateTime rValue)
        {            
            return (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
        }

        public static Tuple<DateTime, int> MonthlyDifferenceFull(this DateTime lValue, DateTime rValue)
        {
            DateTime _lValue = lValue;
            DateTime _rValue = rValue;

            if (!lValue.IsFullPeriodUp(Freq.Month))
                _lValue = lValue.GetLastDayOfMonth(-1);

            if (!rValue.IsFullPeriodDown(Freq.Month))
                _rValue = rValue.GetLastDayOfMonth(1);
                       
            int t = _lValue.MonthlyDifference(_rValue);

            return new Tuple<DateTime, int> (_rValue, _lValue.MonthlyDifference(_rValue));

        }

        public static int QuarterlyDifference(this DateTime lValue, DateTime rValue)
        {
            return lValue.GetLastDayOfQuarter(0).MonthlyDifference(rValue.GetLastDayOfQuarter(0)) / 3;
        }

        public static int QuarterlyDifferenceFull(this DateTime lValue, DateTime rValue)
        {
            DateTime _lValue = lValue;
            DateTime _rValue = rValue;

            if (!lValue.IsFullPeriodUp(Freq.Quarter))
                _lValue = lValue.GetLastDayOfQuarter(-1);

            if (!rValue.IsFullPeriodDown(Freq.Quarter))
                _rValue = rValue.GetLastDayOfQuarter(1);

            return _lValue.QuarterlyDifference(_rValue);
        }

        public static int YearlyDifference(this DateTime lValue, DateTime rValue)
        {
            return (lValue.Year - rValue.Year);
        }

        public static int YearlyDifferenceFull(this DateTime lValue, DateTime rValue)
        {
            DateTime _lValue = lValue;
            DateTime _rValue = rValue;

            if (!lValue.IsFullPeriodUp(Freq.Year))
                _lValue = lValue.GetLastDayOfYear(-1);

            if (!rValue.IsFullPeriodDown(Freq.Year))
                _rValue = rValue.GetLastDayOfYear(1);

            return YearlyDifference(_lValue, _rValue);
        }

        public static double DoAggregation(this List<double> dailyObs, Transf transf)
        {
            if (dailyObs.Count == 0)
                return double.NaN;

            if (transf == Transf.Average)
                return dailyObs.Average();
            else if (transf == Transf.First)
                return dailyObs.First();
            else if (transf == Transf.Last)
                return dailyObs.Last();
            else if (transf == Transf.Max)
                return dailyObs.Max();
            else if (transf == Transf.Min)
                return dailyObs.Min();

            return double.NaN;
        }

        public static DateTime IntToDateTime(this int p, Freq f)
        {
            if (f == Freq.Day )
            {
               return new DateTime(1970,1,1).AddDays(p - 1);
            }
            else if (f == Freq.Month)
            {
                return new DateTime(1970, 1, 1).AddMonths(p - 1).GetLastDayOfMonth(0);
            }
            else if (f == Freq.Quarter)
            {
                return new DateTime(1970, 1, 1).AddMonths((p * 3) - 3).GetLastDayOfQuarter(0);
            }
            else if (f == Freq.Year)
            {
                return new DateTime(1970, 1, 1).AddMonths((p * 12) - 12).GetLastDayOfYear(0);
            }

            return new DateTime(1970,1,1);
        }


        public static int DateTimeToInt(this DateTime p, Freq f)
        {
            if (f == Freq.Day)
            {
                return p.DailyDifference(new DateTime(1970, 1, 1)) + 1 ;
            }
            else if (f == Freq.Month)
            {
                return p.MonthlyDifference(new DateTime(1970, 1, 1)) + 1; 
            }
            else if (f == Freq.Quarter)
            {
                return p.QuarterlyDifference(new DateTime(1970, 1, 1)) + 1;
            }
            else if (f == Freq.Year)
            {
                return p.YearlyDifference(new DateTime(1970, 1, 1)) + 1;
            }

            return 0;
        }

        public static bool IsFullPeriodDown(this DateTime p, Freq f)
        {
            if (f == Freq.Day)
              return true;            
            else if (f == Freq.Month)
              return p.Equals(p.GetFirstDayOfMonth(0));            
            else if (f == Freq.Quarter)
                return p.Equals(p.GetFirstDayOfQuarter(0));        
            else if (f == Freq.Year)
                return p.Equals(p.GetFirstDayOfYear(0));

            return false;
        }

        public static bool IsFullPeriodUp(this DateTime p, Freq f)
        {
            if (f == Freq.Day)
                return true;
            else if (f == Freq.Month)
                return p.Equals(p.GetLastDayOfMonth(0));
            else if (f == Freq.Quarter)
                return p.Equals(p.GetLastDayOfQuarter(0));
            else if (f == Freq.Year)
                return p.Equals(p.GetLastDayOfYear(0));

            return false;
        }

    }
}
