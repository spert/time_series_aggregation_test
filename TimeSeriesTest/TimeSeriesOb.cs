﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeSeriesTest
{
    public class TseriesOb
    {
        public TseriesOb()
        {

        }

        public TseriesOb(double constant)
        {

        }

        public double[] Obs { get; set; }

        public DateTime LastUpdated { get; set; }

        public string ShortName { get; set; }

        public Freq Freq { get; set; }

        public Transf Transf { get; set; }

        public override bool Equals(object obj)
        {
            return this.GetHashCode().Equals(obj.GetHashCode());
        }

        public int FirstPeriod { get; set; } //starts from 1970.01.01  

        public int LastPeriod { get { return FirstPeriod + Obs.GetUpperBound(0); } }

        public DateTime FirstPeriodDate { get { return FirstPeriod.IntToDateTime(Freq); } }

        public DateTime LastPeriodDate { get { return LastPeriod.IntToDateTime(Freq); ; } }


        public override string ToString()
        {
            string s = " ------------------- " + Environment.NewLine;
            s = s + "Observ Series  : " + Environment.NewLine;
            s = s + "ShortName      : " + ShortName + Environment.NewLine;
            s = s + "Frequency      : " + Freq.ToString() + Environment.NewLine;
            s = s + "Transform      : " + Transf.ToString() + Environment.NewLine;
            s = s + "LastUpdated    : " + LastUpdated.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "FirstPeriod    : " + FirstPeriod.ToString() + Environment.NewLine;
            s = s + "FirstPeriodDate: " + FirstPeriodDate.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "LastPeriodDate : " + LastPeriodDate.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "# Obervations  : " + Obs.Length.ToString() + Environment.NewLine;

                        
            for (int i = 0; i < Obs.Length; i++)
            {
                if (i < 3)
                {
                    s = s + i.ToString() + " : " + (FirstPeriod + i).IntToDateTime(Freq).ToString("dd.MM.yyyy") + " : " + Obs[i] + Environment.NewLine;
                }
                if (i == 3)
                {
                    s = s + "..." + Environment.NewLine;
                }
                if (i > Obs.Length - 4)
                {
                    s = s + i.ToString() + " : " + (FirstPeriod + i).IntToDateTime(Freq).ToString("dd.MM.yyyy") + " : " + Obs[i] + Environment.NewLine;
                }
                                               
            }

            return s;

        }

    }
}


