﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeSeriesTest
{
    public class TseriesDict
    {
        public TseriesDict()
        {

        }

        public TseriesDict(double constant)
        {

        }

        public Dictionary<DateTime, double> Obs { get; set; }

        public DateTime LastUpdated { get; set; }

        public string ShortName { get; set; }

        public Freq Freq { get; set; }        

        public Transf Transf { get; set; }

        public override bool Equals(object obj)
        {
            return this.GetHashCode().Equals(obj.GetHashCode());
        }

        public int FirstPeriod { get { return Obs.Keys.First().DateTimeToInt(Freq); }  }     

        public int LastPeriod { get { return Obs.Keys.Last().DateTimeToInt(Freq); } }

        public DateTime FirstPeriodDate { get { return Obs.Keys.First(); } }

        public DateTime LastPeriodDate { get { return Obs.Keys.Last(); } }

        public override string ToString()
        {
            string s = " ------------------- " + Environment.NewLine;
            s = s + "Dict Series    : " + Environment.NewLine;
            s = s + "ShortName      : " + ShortName + Environment.NewLine;
            s = s + "Frequency      : " + Freq.ToString() + Environment.NewLine;
            s = s + "Transform      : " + Transf.ToString() + Environment.NewLine;
            s = s + "LastUpdated    : " + LastUpdated.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "FirstPeriod    : " + FirstPeriod.ToString() + Environment.NewLine;
            s = s + "FirstPeriodDate: " + FirstPeriodDate.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "LastPeriodDate : " + LastPeriodDate.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "# Obervations  : " + Obs.Count.ToString() + Environment.NewLine;
                        

            for (int i = 0; i < Obs.Count; i++)
            {
                if (i < 3)
                {
                    s = s + (i).ToString() + " : " + Obs.ElementAt(i).Key.ToString("dd.MM.yyyy") + " : " + Obs.ElementAt(i).Value.ToString() + Environment.NewLine;
                }
                if (i == 3)
                {
                    s = s + "..." + Environment.NewLine;
                }
                if (i > Obs.Count - 4)
                {
                    s = s + (i).ToString() + " : " + Obs.ElementAt(i).Key.ToString("dd.MM.yyyy") + " : " + Obs.ElementAt(i).Value.ToString() + Environment.NewLine;
                }
            }

            return s;
        }

    }
}
