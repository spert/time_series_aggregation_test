﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TimeSeriesTest
{
    public static class TSeriesConverter
    {
        public unsafe static TseriesOb AggregateInStack(TseriesOb a, Freq f)
        {
            Span<double> dailyObs = stackalloc double[a.Obs.Length];

            for (int i = 0; i < a.Obs.Length; i++)
            {
                dailyObs[i] = a.Obs[i];
            }

            double aggregate = 0;
            int counter = 0;
            int monthCounter = 0;

            Tuple<DateTime, int> mdiff = a.LastPeriodDate.MonthlyDifferenceFull(a.FirstPeriodDate);
            Span<double> monthlyObs = stackalloc double[mdiff.Item2 + 1];

            int daysToEndOfMonth = DateTime.DaysInMonth(mdiff.Item1.Year, mdiff.Item1.Month);
            var beg = mdiff.Item1.GetFirstDayOfMonth(0).DateTimeToInt(Freq.Day);
            var end = mdiff.Item1.GetLastDayOfMonth(mdiff.Item2).DateTimeToInt(Freq.Day);
            var begTimeShift = beg - a.FirstPeriod;
            var endTimeShift = end - a.FirstPeriod;

            for (int i = begTimeShift; i <= endTimeShift; i++)
            {
                double d = dailyObs[i];

                if (!double.IsNaN(d))
                {
                    aggregate = aggregate + d;
                    counter++;
                }

                daysToEndOfMonth--;

                if (daysToEndOfMonth == 0)
                {
                    monthlyObs[monthCounter] = aggregate / counter;
                    monthCounter++;
                    aggregate = 0;
                    counter = 0;
                    DateTime curr = a.FirstPeriodDate.AddDays(i + 1);
                    daysToEndOfMonth = DateTime.DaysInMonth(curr.Year, curr.Month);
                }
            }

            TseriesOb ts = new TseriesOb();
            ts.Freq = Freq.Month;
            ts.ShortName = a.ShortName;
            ts.Transf = a.Transf;
            ts.LastUpdated = a.LastUpdated;
            ts.FirstPeriod = mdiff.Item1.DateTimeToInt(Freq.Month);
            ts.Obs = monthlyObs.ToArray();

            return ts;

        }


        public static TseriesOb AggregateInHeap(TseriesOb a, Freq f)
        {
            double dailyObs = 0;
            int counter = 0;
            int monthCounter = 0;

            Tuple<DateTime, int> mdiff = a.LastPeriodDate.MonthlyDifferenceFull(a.FirstPeriodDate);
            double[] monthlyObs = new double[mdiff.Item2 + 1];

            int daysToEndOfMonth = DateTime.DaysInMonth(mdiff.Item1.Year, mdiff.Item1.Month);
            var beg = mdiff.Item1.GetFirstDayOfMonth(0).DateTimeToInt(Freq.Day);
            var end = mdiff.Item1.GetLastDayOfMonth(mdiff.Item2).DateTimeToInt(Freq.Day);
            var begTimeShift = beg - a.FirstPeriod;
            var endTimeShift = end - a.FirstPeriod;

            for (int i = begTimeShift; i <= endTimeShift; i++)
            {
                double d = a.Obs[i];

                if (!double.IsNaN(d))
                {
                    dailyObs = dailyObs + d;
                    counter++;
                }

                daysToEndOfMonth--;

                if (daysToEndOfMonth == 0)
                {
                    monthlyObs[monthCounter] = dailyObs / counter;
                    monthCounter++;
                    dailyObs = 0;
                    counter = 0;
                    DateTime curr = a.FirstPeriodDate.AddDays(i + 1);
                    daysToEndOfMonth = DateTime.DaysInMonth(curr.Year, curr.Month);
                }
            }

            TseriesOb ts = new TseriesOb();
            ts.Freq = Freq.Month;
            ts.ShortName = a.ShortName;
            ts.Transf = a.Transf;
            ts.LastUpdated = a.LastUpdated;
            ts.FirstPeriod = mdiff.Item1.DateTimeToInt(Freq.Month);
            ts.Obs = monthlyObs.ToArray();

            return ts;

        }


        public static TseriesDict AggregateByDictionary(TseriesDict a, Freq f)
        {

            Dictionary<DateTime, double> monthly = null;

            if (a.Freq == Freq.Day && f == Freq.Month && a.Transf == Transf.Average)
            {
                monthly = a.Obs.Where(y => !double.IsNaN(y.Value)).GroupBy(y => y.Key.GetLastDayOfMonth(0)).ToDictionary(y => y.Key, y => y.Select(v => v.Value).Average());
            }

            if (!a.FirstPeriodDate.IsFullPeriodDown(Freq.Month) &&a.Transf != Transf.Last && a.Transf != Transf.Max && a.Transf != Transf.Min)
            {
                monthly.Remove(monthly.First().Key);
            }

            if (!a.LastPeriodDate.IsFullPeriodUp(Freq.Month) && a.Transf != Transf.First && a.Transf != Transf.Max && a.Transf != Transf.Min)
            { 
                monthly.Remove(monthly.Last().Key);
            }

            TseriesDict ts = new TseriesDict();
            ts.Freq = Freq.Month;
            ts.ShortName = a.ShortName;
            ts.Transf = a.Transf;
            ts.LastUpdated = a.LastUpdated;
            ts.Obs = monthly;
            
            return ts;

        }    }
}
