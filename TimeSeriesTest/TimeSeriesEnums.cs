﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeSeriesTest
{
    public enum Freq { Day = 0, Month = 1, Quarter = 2, Year = 3, None = 4 }

    public enum Transf { Average = 0, Sum = 1, First = 2, Last = 3, Max = 4, Min = 5 }
}
