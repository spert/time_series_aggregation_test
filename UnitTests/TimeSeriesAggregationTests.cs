using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using TimeSeriesTest;

namespace Tests
{
    public class TimeSeriesAggregationTests
    {
        List<TseriesDict> dailyDictSeries = new List<TseriesDict>() ;
        List<TseriesOb> dailyObjectSeries = new List<TseriesOb>();

        [SetUp]
        public void Setup()
        {
            for (int i = 0; i < 61; i++)
            {
                DateTime firstPeriod = new DateTime(1970, 1, 5);
                DateTime lastPeriod = new DateTime(2050, 12, 25);
                Freq f = Freq.Day;
                Transf t = Transf.Average;

                TseriesDict serDict = HelperMethods.GenerateDictSeries(firstPeriod, lastPeriod, f, t);
                TseriesOb serObject = HelperMethods.ConvertDictSeriesToObjectSeries(serDict);
        
                dailyDictSeries.Add(serDict);
                dailyObjectSeries.Add(serObject);
            }

          HelperMethods.ExportSeriesToCsvTable(dailyDictSeries, AppDomain.CurrentDomain.BaseDirectory + "/csvFile.csv");

        }          
        
            
        [Test]
        public void DictSpeedTest()
        {
            List<TseriesDict> resDict= new List<TseriesDict>();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var ser in dailyDictSeries)
            {
                var monthlyDictSeries = TSeriesConverter.AggregateByDictionary(ser, Freq.Month);
                resDict.Add(monthlyDictSeries);
            }

            stopwatch.Stop();
            Debug.WriteLine("Time elapsed Dict: {0}", stopwatch.Elapsed);

        }

        [Test]
        public void HeapSpeedTest()
        {
            List<TseriesOb> resOb = new List<TseriesOb>();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var ser in dailyObjectSeries)
            {
                var monthlyObjectSeries = TSeriesConverter.AggregateInHeap(ser, Freq.Month);
                resOb.Add(monthlyObjectSeries);
            }

            stopwatch.Stop();
            Debug.WriteLine("Time elapsed Object: {0}", stopwatch.Elapsed);
            Debug.WriteLine(resOb[0]);


        }

        [Test]
        public void StackSpeedTest()
        {
            List<TseriesOb> resOb = new List<TseriesOb>();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var ser in dailyObjectSeries)
            {
                Debug.WriteLine(ser.ToString());

                var monthlyObjectSeries = TSeriesConverter.AggregateInStack(ser, Freq.Month);
                resOb.Add(monthlyObjectSeries);
            }

            stopwatch.Stop();
            Debug.WriteLine("Time elapsed Object: {0}", stopwatch.Elapsed);
            Debug.WriteLine(resOb[0]);
        }              

    }
}