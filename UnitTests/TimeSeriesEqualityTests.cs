﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using TimeSeriesTest;

namespace UnitTests
{
    class TimeSeriesEqualityTests
    {
        TseriesDict DictDaily;
        TseriesDict DictDailyFull;
        TseriesDict DictMonthly;
        TseriesDict DictMonthlyFull;
        TseriesDict DictQuarterly;
        TseriesDict DictQuarterlyFull;
        TseriesDict DictYearly;
        TseriesDict DictYearlyFull;

        TseriesOb ObDaily;
        TseriesOb ObDailyFull;
        TseriesOb ObMonthly;
        TseriesOb ObMonthlyFull;
        TseriesOb ObQuarterly;
        TseriesOb ObQuarterlyFull;
        TseriesOb ObYearly;
        TseriesOb ObYearlyFull;

        [SetUp]
        public void Setup()
        {
            DictDaily = DummySeries.GenerateDailyDummySeries();
            DictDailyFull = DummySeries.GenerateDailyDummySeriesFull();
            DictMonthly = DummySeries.GenerateMonthlyDummySeries();
            DictMonthlyFull = DummySeries.GenerateMonthlyDummySeriesFull();
            DictQuarterly = DummySeries.GenerateQuarterlyDummySeries();
            DictQuarterlyFull = DummySeries.GenerateQuarterlyDummySeriesFull();
            DictYearly = DummySeries.GenerateYearlyDummySeries();
            DictYearlyFull = DummySeries.GenerateYearlyDummySeriesFull();

            ObDaily = HelperMethods.ConvertDictSeriesToObjectSeries(DictDaily);
            ObDailyFull = HelperMethods.ConvertDictSeriesToObjectSeries(DictDailyFull);
            ObMonthly = HelperMethods.ConvertDictSeriesToObjectSeries(DictMonthly);
            ObMonthlyFull = HelperMethods.ConvertDictSeriesToObjectSeries(DictMonthlyFull);
            ObQuarterly = HelperMethods.ConvertDictSeriesToObjectSeries(DictQuarterly);
            ObQuarterlyFull = HelperMethods.ConvertDictSeriesToObjectSeries(DictQuarterlyFull);
            ObYearly = HelperMethods.ConvertDictSeriesToObjectSeries(DictQuarterly);
            ObYearlyFull = HelperMethods.ConvertDictSeriesToObjectSeries(DictQuarterlyFull);

        }

        [Test]
        public void DayToDayAggregationTest()
        {
            TseriesOb t1 = HelperMethods.ConvertDictSeriesToObjectSeries(DictDaily);
            Assert.True(HelperMethods.TimeSeriesAreEqual(DictDaily, t1), "1", null);

            TseriesOb t2 = HelperMethods.ConvertDictSeriesToObjectSeries(DictDailyFull);
            Assert.True(HelperMethods.TimeSeriesAreEqual(DictDailyFull, t2), "2", null);
        }

        [Test]
        public void DayToMonthConversions()
        { 
            TseriesOb t1 = TSeriesConverter.AggregateInStack(ObDaily, Freq.Month);             
            Assert.True(HelperMethods.TimeSeriesAreEqual(DictMonthlyFull, t1), "1", null);

            TseriesOb t2 = TSeriesConverter.AggregateInHeap(ObDaily, Freq.Month);

            Assert.True(HelperMethods.TimeSeriesAreEqual(DictMonthlyFull, t2), "2", null);
        }

        [Test]
        public void RandomDayToMonthConversions()
        {
            DateTime firstPeriod = new DateTime(1979, 5, 5);
            DateTime lastPeriod = new DateTime(2050, 12, 25);
            Freq f = Freq.Day;
            Transf t = Transf.Average;

            TseriesDict serDict = HelperMethods.GenerateDictSeries(firstPeriod, lastPeriod, f, t);
            TseriesOb serOb = HelperMethods.ConvertDictSeriesToObjectSeries(serDict);      
            
            TseriesOb t1 = TSeriesConverter.AggregateInStack(serOb, Freq.Month);
            TseriesDict t2 = TSeriesConverter.AggregateByDictionary(serDict, Freq.Month);

            Assert.True(HelperMethods.TimeSeriesAreEqual(t2, t1), "1", null);
         
        }
    }
}
