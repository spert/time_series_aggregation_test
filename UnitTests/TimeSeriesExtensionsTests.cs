﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using TimeSeriesTest;

namespace UnitTests
{
    class TimeSeriesExtensionsTests
    {        
        DateTime d1 = new DateTime(1970, 1, 10);
        DateTime d2 = new DateTime(1971, 4, 5);
        DateTime d3 = new DateTime(1975, 4, 5);

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void GetLastDayOfMonthTests()
        {
            Assert.True(d1.GetLastDayOfMonth(0).Equals(new DateTime(1970,1,31)), "1", null);
            Assert.True(d1.GetLastDayOfMonth(1).Equals(new DateTime(1970,2,28)), "2", null);
            Assert.True(d1.GetLastDayOfMonth(-1).Equals(new DateTime(1969,12,31)), "3", null);
            Assert.True(d1.GetLastDayOfMonth(-12).Equals(new DateTime(1969,1,31)), "4", null);
        }

        [Test]
        public void GetFirstDayOfMonthTests()
        {
            Assert.True(d1.GetFirstDayOfMonth(0).Equals(new DateTime(1970, 1, 1)), "1", null);
            Assert.True(d1.GetFirstDayOfMonth(1).Equals(new DateTime(1970, 2, 1)), "2", null);
            Assert.True(d1.GetFirstDayOfMonth(-1).Equals(new DateTime(1969, 12, 1)), "3", null);
            Assert.True(d1.GetFirstDayOfMonth(-12).Equals(new DateTime(1969, 1, 1)), "4", null);
        }

        [Test]
        public void GetLastDayOfQuarterTests()
        {
            Assert.True(d1.GetLastDayOfQuarter(0).Equals(new DateTime(1970, 3, 31)),"1",null);
            Assert.True(d1.GetLastDayOfQuarter(1).Equals(new DateTime(1970, 6, 30)), "2", null);
            Assert.True(d1.GetLastDayOfQuarter(-1).Equals(new DateTime(1969, 12, 31)), "3", null);
            Assert.True(d1.GetLastDayOfQuarter(-4).Equals(new DateTime(1969, 3, 31)), "4", null);
        }

        [Test]
        public void GetFirstDayOfQuarterTests()
        {
            Assert.True(d1.GetFirstDayOfQuarter(0).Equals(new DateTime(1970, 1, 1)), "1", null);
            Assert.True(d1.GetFirstDayOfQuarter(1).Equals(new DateTime(1970, 4, 1)), "2", null);
            Assert.True(d1.GetFirstDayOfQuarter(-1).Equals(new DateTime(1969, 10, 1)), "3", null);
            Assert.True(d1.GetFirstDayOfQuarter(-4).Equals(new DateTime(1969, 1, 1)), "4", null);
        }

        [Test]
        public void GetLastDayOfYearTests()
        {
            Assert.True(d1.GetLastDayOfYear(0).Equals(new DateTime(1970, 12, 31)), "1", null);
            Assert.True(d1.GetLastDayOfYear(1).Equals(new DateTime(1971, 12, 31)), "2", null);
            Assert.True(d1.GetLastDayOfYear(-1).Equals(new DateTime(1969, 12, 31)), "3", null);
            Assert.True(d1.GetLastDayOfYear(-4).Equals(new DateTime(1966, 12, 31)), "4", null);
        }

        [Test]
        public void GetFirstDayOfYearTests()
        {
            Assert.True(d1.GetFirstDayOfYear(0).Equals(new DateTime(1970, 1, 1)), "1", null);
            Assert.True(d1.GetFirstDayOfYear(1).Equals(new DateTime(1971, 1, 1)), "2", null);
            Assert.True(d1.GetFirstDayOfYear(-1).Equals(new DateTime(1969, 1, 1)), "3", null);
            Assert.True(d1.GetFirstDayOfYear(-4).Equals(new DateTime(1966, 1, 1)), "4", null);
        }
        
        [Test]
        public void DifferencesFullTests()
        {
            Assert.True(d2.DailyDifference(d1) == 450, "1", null);
            Assert.True(d2.DailyDifferenceFull(d1) == 450, "2", null);
            Assert.True(d2.MonthlyDifference(d1) == 15, "3", null);
            Assert.True(d2.MonthlyDifferenceFull(d1).Item2 == 13, "4", null);
            Assert.True(d2.QuarterlyDifference(d1) == 5, "5", null);
            Assert.True(d2.QuarterlyDifferenceFull(d1) == 3, "6", null);
            Assert.True(d3.YearlyDifference(d1) == 5, "7", null);
            Assert.True(d3.YearlyDifferenceFull(d1) == 3, "8", null);

            Assert.True(d2.MonthlyDifference(d2) == 0, "9", null);

        }

        [Test]
        public void IntToDateTimeTests()
        {
            Assert.True(Int32.Parse("460").IntToDateTime(Freq.Day).Equals(new DateTime(1971,4,5)), "1", null);
            Assert.True(Int32.Parse("16").IntToDateTime(Freq.Month).Equals(new DateTime(1971, 4, 30)), "2", null);
            Assert.True(Int32.Parse("6").IntToDateTime(Freq.Quarter).Equals(new DateTime(1971, 6, 30)), "3", null);
            Assert.True(Int32.Parse("2").IntToDateTime(Freq.Year).Equals(new DateTime(1971, 12, 31)), "4", null);

        }
        
        [Test]
        public void DateTimeToIntTests()
        {
            Assert.True(new DateTime(1971, 4, 5).DateTimeToInt(Freq.Day).Equals(460), "1", null);
            Assert.True(new DateTime(1971, 4, 20).DateTimeToInt(Freq.Month).Equals(16), "2", null);
            Assert.True(new DateTime(1971, 6, 30).DateTimeToInt(Freq.Quarter).Equals(6), "3", null);
            Assert.True(new DateTime(1971, 12, 31).DateTimeToInt(Freq.Year).Equals(2), "4", null);

        }

        [Test]
        public void DateTimeAndIntConversions()
        {
            DateTime d = new DateTime(1970, 1, 1);
            Assert.True(d.DateTimeToInt(Freq.Day) == 1);
            Assert.True(d.DateTimeToInt(Freq.Month) == 1);
            Assert.True(d.DateTimeToInt(Freq.Quarter) == 1);
            Assert.True(d.DateTimeToInt(Freq.Year) == 1);

            int i = 1;
            Assert.True(i.IntToDateTime(Freq.Day) == new DateTime(1970, 1, 1));
            Assert.True(i.IntToDateTime(Freq.Month) == new DateTime(1970, 1, 31));
            Assert.True(i.IntToDateTime(Freq.Quarter) == new DateTime(1970, 3, 31));
            Assert.True(i.IntToDateTime(Freq.Year) == new DateTime(1970, 12, 31));

            DateTime v = new DateTime(1976, 5, 10);
            Assert.True(v.DateTimeToInt(Freq.Day) == 2322);
            Assert.True(v.DateTimeToInt(Freq.Month) == 77);
            Assert.True(v.DateTimeToInt(Freq.Quarter) == 26);
            Assert.True(v.DateTimeToInt(Freq.Year) == 7);

            int j = 2322;
            Assert.True(j.IntToDateTime(Freq.Day) == new DateTime(1976, 05, 10));
            j = 77;
            Assert.True(j.IntToDateTime(Freq.Month) == new DateTime(1976, 05, 31));
            j = 26;
            Assert.True(j.IntToDateTime(Freq.Quarter) == new DateTime(1976, 06, 30));
            j = 7;
            Assert.True(j.IntToDateTime(Freq.Year) == new DateTime(1976, 12, 31));
        }


    }
}
